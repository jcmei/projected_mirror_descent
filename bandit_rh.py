import numpy as np
import math

import matplotlib
# Force matplotlib to not use and Xwindows backend.
matplotlib.use('Agg')

import matplotlib.pyplot as plt

folderpath = './testmethod2/'
n = 200
MAX_IND = 5
k = 10
MAX_ITER = 2000
EPSILON = 0.0005
LEARNING_RATE = 0.1
LEARNING_RATE2 = 0.1
LEARNING_RATE3 = 0.5
LEARNING_RATE4 = 0.5
SIMULATION_NUM = 10
ITERATION_NUM = 50
EXPERIMENT_NUM = 5

NUM_METHOD = 5

def softmax(q):
    q = q - np.max(q)
    return np.exp(q)/ np.sum(np.exp(q))

def generate_action(pi):
    a = np.random.choice(np.arange(0, n), p=pi)
    return a

def get_policy_from_parameter(x, theta): 
    q = np.dot(x, theta)
    return softmax(q)

def fixedlog(q):
    q[q<=0]= 1e-10
    return q

def initialize():  # initialize reward, pi, x, theta
    reward = 1.0+ 0.2*np.random.randn(n)
    reward = np.power(reward, 2)
    max_r = np.amax(reward)
    print(max_r)

    for i in range(MAX_IND):
        a = np.random.randint(0, n)
        reward[a] = max_r
    x = np.random.rand(n, k)
    theta = np.zeros(k)
    pi = get_policy_from_parameter(x, theta)
    #pi_bar = np.ones(n) * (1.0 / n)

    #cumulative_sample_reward = np.zeros(n)

    return reward, pi, x, theta
##################


def unbiased_estimation(reward, pi):  # estimation = (sample mean reward) / pi
    unbiased_reward = np.zeros(n)
    for i in range(SIMULATION_NUM):
        a = generate_action(pi)
        unbiased_reward[a] = unbiased_reward[a] + reward[a] / pi[a]
    unbiased_reward = unbiased_reward / SIMULATION_NUM
    return unbiased_reward

def unbiased_exp_estimation(reward, pi, tau=0.1):
    biased_exp_reward = np.zeros(n)
    unbiased_reward = np.zeros(n)
    for i in range(SIMULATION_NUM):
        a = generate_action(pi)
        biased_exp_reward[a] = biased_exp_reward[a] + np.exp(reward[a] / tau) / pi[a]
        unbiased_reward[a] = unbiased_reward[a] + reward[a] / pi[a]
    biased_exp_reward = biased_exp_reward / SIMULATION_NUM
    unbiased_reward = unbiased_reward / SIMULATION_NUM
    return unbiased_reward, biased_exp_reward

def construct_policy(r1, r2):
    r_sum = r1+r2
    bar_pi = softmax(r_sum)
    return bar_pi

def reward_evaluation(reward, x, theta):
    pi = get_policy_from_parameter(x, theta)
    reward_eval = np.dot(reward, pi)
    return reward_eval


def reward_parameter_gradient(reward, x, theta):
    pi = get_policy_from_parameter(x, theta)
    gradient = np.dot(np.transpose(x), np.multiply(pi, reward) - np.inner(pi, reward)*pi)
    return gradient


def reward_plus_kl_gradient(reward, bar_pi, x, theta, epsilon, learning_rate, tau = 0.1):
    grad = - reward_parameter_gradient(reward, x, theta) + tau * kl_parameter_gradient(bar_pi, x, theta)
    theta = theta - learning_rate * grad
    pi = get_policy_from_parameter(x, theta)
    return pi, theta


def kl_divergence(bar_pi, x, theta):
    pi = get_policy_from_parameter(x, theta)
    kl = 0
    for i in range(n):
        if pi[i] == 0:
            continue
        else:
            kl = kl - bar_pi[i] * np.log(pi[i])
        if bar_pi[i] == 0:
            continue
        else:
            kl = kl + bar_pi[i] * np.log(bar_pi[i])
    return kl


def kl_parameter_gradient(bar_pi, x, theta):
    pi = get_policy_from_parameter(x, theta)
    gradient = np.dot(np.transpose(x), pi-bar_pi)
    return gradient


def ment_gradient(reward, x, theta, learning_rate, tau = 0.1):
    pi = get_policy_from_parameter(x, theta)
    r = reward - tau * np.log(fixedlog(pi))
    grad = - reward_parameter_gradient(r, x, theta)
    theta = theta - learning_rate * grad
    pi = get_policy_from_parameter(x, theta)
    return pi, theta


def kl_projection(bar_pi, x, theta, epsilon, learning_rate, num_iter):
    while True:
        prev_kl = kl_divergence(bar_pi, x, theta)
        grad = kl_parameter_gradient(bar_pi, x, theta)
        theta = theta - learning_rate * grad
        curr_kl = kl_divergence(bar_pi, x, theta)
        learning_rate = learning_rate * 0.999
        if abs(prev_kl - curr_kl) < epsilon:
            break
    pi = get_policy_from_parameter(x, theta)
    return pi, theta

######### Methods 
def pi_bar_proj(reward, pi, tau, x, theta, lr):
    unbiased_reward= unbiased_estimation(reward, pi)
    pi_bar = construct_policy(unbiased_reward/tau, np.log(fixedlog(pi)))
    pi, theta = kl_projection(pi_bar, x, theta, EPSILON, lr, ITERATION_NUM)
    diff = np.inner(reward, pi_bar - pi)
    return pi, pi_bar, theta, diff
def urex(reward, pi, tau, x, theta, lr):
    ur= unbiased_estimation(reward, pi)
    pi_bar = construct_policy(ur/tau, np.zeros(n))
    pi, theta = reward_plus_kl_gradient(ur, pi_bar, x, theta, EPSILON, lr)
    diff = np.inner(reward, pi_bar - pi)
    return pi, theta, diff

def ment(reward, pi, tau, x, theta, lr):
    ur= unbiased_estimation(reward, pi)
    pi, theta = ment_gradient(ur, x, theta, lr, tau)
    return pi, theta

def testmethod(reward, pi, x, theta, lr):
    unbiased_reward= unbiased_estimation(reward, pi)
    pi_bar = construct_policy(np.log(fixedlog(unbiased_reward)), np.log(fixedlog(pi)))
    pi, theta = kl_projection(pi_bar, x, theta, EPSILON, lr, ITERATION_NUM)
    diff = np.inner(reward, pi_bar - pi)
    return pi, theta, diff

#####
def pi_bar_proj_biased(reward, pi, tau, x, theta, lr):
    unbiased_reward, biased_exp_reward= unbiased_exp_estimation(reward, pi, tau)
    pi_bar = construct_policy(np.log(fixedlog(biased_exp_reward)), np.log(fixedlog(pi)))
    pi, theta = kl_projection(pi_bar, x, theta, EPSILON, lr, ITERATION_NUM)
    diff = np.inner(reward, pi_bar - pi)
    return pi, pi_bar, theta, diff

def urex_biased(reward, pi, tau, x, theta, lr):
    unbiased_reward, biased_exp_reward= unbiased_exp_estimation(reward, pi, tau)
    pi_bar = construct_policy(np.log(fixedlog(biased_exp_reward)), np.zeros(n))
    pi, theta = reward_plus_kl_gradient(ur, pi_bar, x, theta, EPSILON, lr)
    diff = np.inner(reward, pi_bar - pi)
    return pi, theta, diff

def testmethod_biased(reward, pi, x, theta, lr):
    unbiased_reward, biased_exp_reward= unbiased_exp_estimation(reward, pi,tau)
    pi_bar = construct_policy(np.log(fixedlog(np.log(fixedlog(biased_exp_reward)))), np.log(fixedlog(pi)))
    pi, theta = kl_projection(pi_bar, x, theta, EPSILON, lr, ITERATION_NUM)
    diff = np.inner(reward, pi_bar - pi)
    return pi, theta, diff

############    
def run():
    results = np.zeros([NUM_METHOD, MAX_ITER])
    #diff_res = np.zeros([NUM_METHOD, MAX_ITER])
    results_biased = np.zeros([NUM_METHOD, MAX_ITER])
    #diff_res_biased = np.zeros([NUM_METHOD, MAX_ITER])
    
    reward_mean, pi, x, theta = initialize()
    pi2 = pi
    pi_bar2 = pi
    theta2 = theta
    pi3 = pi
    theta3 = theta
    pi4 = pi
    theta4 = theta
    pi_test = pi
    theta_test = theta

    unbiased_reward = np.zeros(n)
    unbiased_reward2 = np.zeros(n)
    ur = np.zeros(n)
    ur4 = np.zeros(n)
    unbiased_reward_test = np.zeros(n)

    index = 0
    lr = LEARNING_RATE
    lr2 = LEARNING_RATE2
    lr3 = LEARNING_RATE3
    lr4 = LEARNING_RATE4
    for step in range(1, MAX_ITER):

        tau = 0.1#0.05*np.sqrt(step)
        reward = np.random.normal(reward_mean, 0.1)
        lr3 = lr3 * 0.999
        lr4 = lr4 * 0.999

        # update for pi_bar projection
        pi, pi_bar, theta, diff0= pi_bar_proj(reward, pi, tau, x, theta, lr)
        pi2, pi_bar2, theta2, diff2= pi_bar_proj(reward, pi_bar2, tau, x, theta2, lr2)
        pi3, theta3, diff3 = urex(reward, pi3, tau, x, theta3, lr3)
        pi4, theta4 = ment(reward, pi4, tau, x, theta4, lr4)
        pi_test, theta_test, diff_test= testmethod(reward, pi_test, x, theta_test, lr)

        pi_biased, pi_bar_biased, theta_biased, diff0_biased= pi_bar_proj(reward, pi_biased, tau, x, theta_biased, lr)
        pi2_biased, pi_bar2_biased, theta2_biased, diff2_biased= pi_bar_proj(reward, pi_bar2_biased, tau, x, theta2_biased, lr2)
        pi3_biased, theta3_biased, diff3_biased = urex(reward, pi3_biased, tau, x, theta3_biased, lr3)
        pi_test_biased, theta_test_biased, diff_test_biased= testmethod(reward, pi_test_biased, x, theta_test_biased, lr)

        # get estimation
        r = reward_evaluation(reward, x, theta)
        r2 = reward_evaluation(reward, x, theta2)
        r3 = reward_evaluation(reward, x, theta3)
        r4 = reward_evaluation(reward, x, theta4)
        r_test = reward_evaluation(reward, x, theta_test)

        r_biased = reward_evaluation(reward, x, theta_biased)
        r2_biased = reward_evaluation(reward, x, theta2_biased)
        r3_biased = reward_evaluation(reward, x, theta3_biased)
        r_test_biased = reward_evaluation(reward, x, theta_test_biased)

        results[0][step] = results[0][step-1] + r
        results[1][step] = results[1][step-1] +r2
        results[2][step] = results[2][step-1] +r3
        results[3][step] = results[3][step-1] +r4
        results[4][step] = results[4][step-1] + r_test
        results_biased[0][step] = results_biased[0][step-1] + r_biased
        results_biased[1][step] = results_biased[1][step-1] +r2_biased
        results_biased[2][step] = results_biased[2][step-1] +r3_biased
        results_biased[4][step] = results_biased[4][step-1] + r_test_biased

        '''
        diff_res[0][step] =  diff0
        diff_res[1][step] =  diff2
        diff_res[2][step] =  diff3
        diff_res[3][step] =  0
        diff_res[4][step] =  diff_test
        '''
        if step%50 ==1:
            print('~~~~~~~~~~~~~~~~~~~~')
            print(step)
            print(r)
            print(r2)
            print(r3)
            print(r4)
            print(r_test)
            print(r_biased)
            print(r2_biased)
            print(r3_biased)
            print(r_test_biased)
            print('~~~~~~~~~~~~~~~~~~~~')
            '''
            print(np.max(pi_bar))
            print(np.max(pi_bar2))
            print(np.max(pi_bar3))
            print(np.max(pi_bar_test))
            print('~~~~~~~~~~~~~~~~~~~~')
            '''
    return results,results_biased


def draw_graph(name, result, error_bar=None):
    r = result[0]
    r2 = result[1]
    r3 = result[2]
    r4 = result[3]
    r_test = result[4]
    if error_bar is None:
        l1, = plt.plot(r[::5], 'r', label='pi_bar_proj')
        l2, = plt.plot(r2[::5], 'g', label='pt_proj')
        l3, = plt.plot(r3[::5], 'b', label='urex')
        l4, = plt.plot(r4[::5], 'm', label='ment')
        l_test, = plt.plot(r_test[::5], 'k', label='test_method') 
        plt.legend(handles=[l1, l2, l3, l4, l_test])
        plt.savefig(folderpath+name)
        plt.clf()
        plt.close('all')        


def save_result(name, result):
    f = file('./testmethod/'+name+'.npy', "wb")
    np.save(f, result)
    f.close()

def draw_error_bar(name, result):
    r0= result[0].reshape([MAX_ITER, EXPERIMENT_NUM])
    r1= result[1].reshape([MAX_ITER, EXPERIMENT_NUM])
    r2= result[2].reshape([MAX_ITER, EXPERIMENT_NUM])
    r3= result[3].reshape([MAX_ITER, EXPERIMENT_NUM])
    r4= result[4].reshape([MAX_ITER, EXPERIMENT_NUM])

    r0_mean = np.mean(r0, axis = 1)
    r1_mean = np.mean(r1, axis = 1)
    r2_mean = np.mean(r2, axis = 1)
    r3_mean = np.mean(r3, axis = 1)
    r4_mean = np.mean(r4, axis = 1)
    std0 = np.std(r0,axis = 1)
    std1 = np.std(r1,axis = 1)
    std2 = np.std(r2,axis = 1)
    std3 = np.std(r3,axis = 1)
    std4 = np.std(r4,axis = 1)

    l0, = plt.plot(r0_mean[::5], 'r', label='pi_bar_proj')
    l0_up, = plt.plot(r0_mean[::5]+std0[::5], 'r--')
    l0_low, = plt.plot(r0_mean[::5]-std0[::5], 'r--')
    #area0 = plt.fill_between(l0_low, l0_up, facecolor="gray", alpha=0.15)

    l1, = plt.plot(r1_mean[::5], 'g', label='pt_proj')
    l1_up, = plt.plot(r1_mean[::5]+std1[::5], 'g--')
    l1_low, = plt.plot(r1_mean[::5]-std1[::5], 'g--')

    l2, = plt.plot(r2_mean[::5], 'b', label='urex')
    l2_up, = plt.plot(r2_mean[::5]+std2[::5], 'b--')
    l2_low, = plt.plot(r2_mean[::5]-std2[::5], 'b--')

    l3, = plt.plot(r3_mean[::5], 'm', label='ment')
    l3_up, = plt.plot(r3_mean[::5]+std3[::5], 'm--')
    l3_low, = plt.plot(r3_mean[::5]-std3[::5], 'm--')

    l4, = plt.plot(r4_mean[::5], 'k', label='testmethod')
    l4_up, = plt.plot(r4_mean[::5]+std4[::5], 'k--')
    l4_low, = plt.plot(r4_mean[::5]-std4[::5], 'k--')

    plt.legend(handles=[l0, l1, l2, l3, l4])
    plt.savefig(folderpath+name)
    plt.clf()
    plt.close('all')

if __name__ == '__main__':
    result = np.zeros([NUM_METHOD, MAX_ITER, EXPERIMENT_NUM])
    result_biased = np.zeros([NUM_METHOD, MAX_ITER, EXPERIMENT_NUM])
    for i in range(EXPERIMENT_NUM):
        name = 'test_result_{}'.format(i)
        x, x_biased = run()
        save_result(name, x)
        save_result(name+'_biased', x_biased)
        result[:,:,i] = x
        result_biased[:,:,i] = x_biased
        draw_graph(name, x)
        draw_graph(name+'_biased', x_biased)
        #draw_graph('diff_result_{}'.format(i), diff_res)


    #result = result / EXPERIMENT_NUM
    #print(result)
    draw_error_bar('test_result_avg', result)
    draw_error_bar('test_result_avg_biased', result_biased)




'''
        unbiased_reward= unbiased_estimation(reward, pi)
        pi_bar = construct_policy(unbiased_reward/tau, np.log(fixedlog(pi)))
        pi, theta = kl_projection(pi_bar, x, theta, EPSILON, lr, ITERATION_NUM)
        diff0 = np.inner(reward, pi_bar - pi)
        
        # update for p_t projection
        unbiased_reward2= unbiased_estimation(reward, pi_bar2)
        pi_bar2 = construct_policy(unbiased_reward2/tau, np.log(fixedlog(pi_bar2)))
        pi2, theta2 = kl_projection(pi_bar2, x, theta2, EPSILON, lr2, ITERATION_NUM)
        diff2 = np.inner(reward, pi_bar2 - pi2)
        
        # update for urex
        ur= unbiased_estimation(reward, pi3)
        pi_bar3 = construct_policy(ur/tau, np.zeros(n))
        pi3, theta3 = reward_plus_kl_gradient(ur, pi_bar3, x, theta3, EPSILON, lr3)
        diff3 = np.inner(reward, pi_bar3 - pi3)
        
        # update for ment
        ur4= unbiased_estimation(reward, pi4)
        pi4, theta4 = ment_gradient(ur4, x, theta4, lr4, 0.1)
        

        # log r update
        unbiased_reward_test= unbiased_estimation(reward, pi_test)
        pi_bar_test = construct_policy(np.log(fixedlog(unbiased_reward_test)), np.log(fixedlog(pi_test)))
        pi_test, theta_test = kl_projection(pi_bar_test, x, theta_test, EPSILON, lr, ITERATION_NUM)
        diff_test = np.inner(reward, pi_bar_test - pi_test)
'''