import numpy as np
import math

import matplotlib
# Force matplotlib to not use and Xwindows backend.
matplotlib.use('Agg')

import matplotlib.pyplot as plt

n = 10000
k = 10
MAX_ITER = 15000
EPSILON = 0.0005
LEARNING_RATE = 0.1
LEARNING_RATE2 = 0.1
LEARNING_RATE3 = 0.5
LEARNING_RATE4 = 0.5
SIMULATION_NUM = 10
ITERATION_NUM = 50
EXPERIMENT_NUM = 5
tau = 0.1


def initialize():  # initialize reward, pi, x, theta
    reward = np.random.rand(n)
    reward = np.power(reward, 2)
    # reward = np.array([0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1,
    # 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 10.0, 10.0, 10.0])
    max_r = np.amax(reward)
    print(max_r)

    for i in range(5):
        a = np.random.randint(0, n)
        reward[a] = max_r
    x = np.random.rand(n, k)
    theta = np.zeros(k)
    pi = get_policy_from_parameter(x, theta)
    pi_bar = np.ones(n) * (1.0 / n)

    cumulative_sample_reward = np.zeros(n)

    return reward, pi, pi_bar, x, theta, cumulative_sample_reward


def get_policy_from_parameter(x, theta):  # pi \prop exp(x*theta)
    q = np.dot(x, theta) / tau
    exp_q = np.exp(q)
    normalizer = np.sum(exp_q)
    pi = exp_q / normalizer
    return pi


def unbiased_estimation(reward, pi):  # estimation = (sample mean reward) / pi
    unbiased_reward = np.zeros(n)
    for i in range(SIMULATION_NUM):
        a = generate_action(pi)
        unbiased_reward[a] = unbiased_reward[a] + reward[a] / pi[a]
    unbiased_reward = unbiased_reward / SIMULATION_NUM
    return unbiased_reward


def unbiased_exp_estimation(reward, pi):
    unbiased_exp_reward = np.zeros(n)
    unbiased_reward = np.zeros(n)
    for i in range(SIMULATION_NUM):
        a = generate_action(pi)
        unbiased_exp_reward[a] = unbiased_exp_reward[a] + np.exp(reward[a] / tau) / pi[a]
        unbiased_reward[a] = unbiased_reward[a] + reward[a] / pi[a]
    unbiased_exp_reward = unbiased_exp_reward / SIMULATION_NUM
    unbiased_reward = unbiased_reward / SIMULATION_NUM
    return unbiased_reward, unbiased_exp_reward


def construct_policy(exp_r, pi):
    bar_pi = np.multiply(pi, exp_r)
    normalizer = np.sum(bar_pi)
    bar_pi = bar_pi / normalizer
    return bar_pi


def construct_exp_policy(u, pi):  # bar_pi \prop pi*exp(estimation)
    exp_u = np.exp(u / tau)
    bar_pi = np.multiply(pi, exp_u)
    normalizer = np.sum(bar_pi)
    bar_pi = bar_pi / normalizer
    return bar_pi


def reward_evaluation(reward, x, theta):
    pi = get_policy_from_parameter(x, theta)
    reward_eval = np.dot(reward, pi)
    return reward_eval


def reward_parameter_gradient(reward, x, theta):
    pi = get_policy_from_parameter(x, theta)
    gradient = (1 / tau) * np.dot(np.transpose(x), np.multiply(pi, reward - np.dot(pi, reward)))
    return gradient


def reward_plus_kl_gradient(reward, bar_pi, x, theta, epsilon, learning_rate):
    grad = - reward_parameter_gradient(reward, x, theta) + tau * kl_parameter_gradient(bar_pi, x, theta)
    theta = theta - learning_rate * grad
    pi = get_policy_from_parameter(x, theta)
    return pi, theta


def kl_divergence(bar_pi, x, theta):
    pi = get_policy_from_parameter(x, theta)
    kl = 0
    for i in range(n):
        kl = kl - bar_pi[i] * math.log(pi[i])
        if bar_pi[i] == 0:
            continue
        else:
            kl = kl + bar_pi[i] * math.log(bar_pi[i])
    return kl


def kl_parameter_gradient(bar_pi, x, theta):
    pi = get_policy_from_parameter(x, theta)
    gradient = np.dot(np.transpose(x), pi - bar_pi)
    return gradient


def ment_gradient(reward, x, theta, learning_rate):
    p = get_policy_from_parameter(x, theta)
    r = reward - tau * np.log(p)
    grad = - reward_parameter_gradient(r, x, theta)
    theta = theta - learning_rate * grad
    pi = get_policy_from_parameter(x, theta)
    return pi, theta


def kl_projection(bar_pi, x, theta, epsilon, learning_rate, num_iter):
    while True:
        prev_kl = kl_divergence(bar_pi, x, theta)
        grad = kl_parameter_gradient(bar_pi, x, theta)
        theta = theta - learning_rate * grad
        curr_kl = kl_divergence(bar_pi, x, theta)
        learning_rate = learning_rate * 0.999
        if abs(prev_kl - curr_kl) < epsilon:
            break
    pi = get_policy_from_parameter(x, theta)
    return pi, theta


def generate_action(pi):
    a = np.random.choice(np.arange(0, n), p=pi)
    return a


def run():
    results = np.zeros([4, MAX_ITER])
    reward, pi, pi_bar, x, theta, cumulative_sample_reward = initialize()
    pi2 = pi
    pi_bar2 = pi_bar
    theta2 = theta
    pi3 = pi
    pi_bar3 = pi_bar
    theta3 = theta
    uniform_distribution = pi_bar
    pi4 = pi
    pi_bar4 = pi_bar
    theta4 = theta
    index = 0
    exp_reward = np.exp(reward / tau)
    lr = LEARNING_RATE
    lr2 = LEARNING_RATE2
    lr3 = LEARNING_RATE3
    lr4 = LEARNING_RATE4
    for step in range(MAX_ITER):
        #lr = lr * 0.999
        #lr2 = lr2 * 0.999
        
        # update for pi_bar projection
        _, unbiased_exp_reward = unbiased_exp_estimation(reward, pi)
        pi_bar = construct_policy(np.exp(reward/tau), pi)
        # pi, theta = reward_plus_kl_gradient(reward, pi_bar, x, theta, EPSILON, LEARNING_RATE)
        pi, theta = kl_projection(pi_bar, x, theta, EPSILON, lr, ITERATION_NUM)

        """""
        # update for p_t projection
        _, unbiased_exp_reward2 = unbiased_exp_estimation(reward, pi_bar2)
        pi_bar2 = construct_policy(unbiased_exp_reward2, pi_bar2)
        pi2, theta2 = kl_projection(pi_bar2, x, theta2, EPSILON, lr2, ITERATION_NUM)

        lr3 = lr3 * 0.999
        lr4 = lr4 * 0.999
        # update for urex
        ur, u_exp_r = unbiased_exp_estimation(reward, pi3)
        pi_bar3 = construct_policy(u_exp_r, uniform_distribution)
        pi3, theta3 = reward_plus_kl_gradient(ur, pi_bar3, x, theta3, EPSILON, lr3)

        # update for ment
        ur, _ = unbiased_exp_estimation(reward, pi4)
        pi4, theta4 = ment_gradient(ur, x, theta4, lr4)
        """""

        # get estimation
        r = reward_evaluation(reward, x, theta)
        r2 = reward_evaluation(reward, x, theta2)
        r3 = reward_evaluation(reward, x, theta3)
        r4 = reward_evaluation(reward, x, theta4)

        results[0][step] = r
        results[1][step] = r2
        results[2][step] = r3
        results[3][step] = r4

        print('~~~~~~~~~~~~~~~~~~~~')
        print(step)
        print(r)
        #print(r2)
        #print(r3)
        #print(r4)
        print('~~~~~~~~~~~~~~~~~~~~')

    return results


def draw_graph(name, result, error_bar=None):
    r = result[0]
    r2 = result[1]
    r3 = result[2]
    r4 = result[3]
    if error_bar is None:
        l1, = plt.plot(r, 'r', label='pi_bar_proj')
        l2, = plt.plot(r2, 'g', label='pt_proj')
        l3, = plt.plot(r3, 'b', label='urex')
        l4, = plt.plot(r4, 'm', label='ment')
        plt.legend(handles=[l1, l2, l3, l4])
        plt.savefig(name)
        plt.clf()
        plt.close('all')        


def save_result(name, result):
    f = file(name+'.npy', "wb")
    np.save(f, result)
    f.close()


if __name__ == '__main__':
    result = np.zeros([4, MAX_ITER])
    for i in range(EXPERIMENT_NUM):
        name = 'result_{}'.format(i)
        x = run()
        save_result(name, x)
        result = result + x
        draw_graph(name, x)
    result = result / EXPERIMENT_NUM
    print(result)
    draw_graph('result_avg', result)
